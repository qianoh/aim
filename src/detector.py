from lane import lane
class detector:
	def __init__(self,did,lane,pos,freq,output):
		self.did=did
		self.lane=lane
		self.pos=pos
		self.freq=freq
		self.output=output
		
class ildetector(detector):
	def toXMLString(self):
		detectorString="<inductionLoop"+"  "+"id=\""+self.did+"\" "+"lane=\""+self.lane+"\" "+"pos=\""+str(self.pos)+"\" "+"freq=\""+str(self.freq)+"\""+" file=\""+self.output+"\""+""" friendlyPos="true"/>\n"""
		return detectorString
		
class e3detector(detector):
	def __init__(self,did,entryLane,entryPos,exitLane,exitPos,freq,output):
		self.did=did
		self.entryLane=entryLane
		self.entryPos=entryPos
		self.exitLane=exitLane
		self.exitPos=exitPos
		self.freq=freq
		self.output=output
	def toXMLString(self):
		detectorString="<entryExitDetector id=\""+self.did+"\" freq=\""+str(self.freq)+"\" file=\""+self.output+"\">"
		detectorString+="\n"
		detectorString+="<detEntry lane=\""+self.entryLane.lid+"\" pos=\""+str(self.entryPos)+"\"/>\n"
		detectorString+="<detExit lane=\""+self.exitLane.lid+"\" pos=\""+str(self.exitPos)+"\"/>\n"
		detectorString+="</entryExitDetector>\n"
		return detectorString
