
"""
Parameter of simulation

"""

DCC=4.0
ACC=2.0
MIN_INTERVAL=0.2
MAX_SPEED=15.0
#resolution of simulation, here the resolution is 0.1
STEPPERSECOND=10.0
networkSize=3
p_left = 0.3
p_right = 0
p_straight = 1
p_exit=0.0
p_arrive_automated=0.03
p_arrive_cooperative=0.01
p_arrive_legacy=0.01
laneNumber=3
typeOfIntersection="priority"

TOTAL_VEHICLE_ARRIVE_TIME=2000#vehicle will arrive in first xxx seconds
TOTAL_RUN_TIME=2000

START_LANE_LENGTH=50

#Collision hemisphere
COLLISION_R=5
THRESHOLD=5
# the port used for communicating with your sumo instance
PORT = 8813

#file locations
#file location
NETWORK_LOCATION='../data/multiIntersection/'
NETWORK_NAME='cross'
NODE_FILE=NETWORK_LOCATION+NETWORK_NAME+".nod.xml"
EDGE_FILE=NETWORK_LOCATION+NETWORK_NAME+".edg.xml"
CONNECTION_FILE=NETWORK_LOCATION+NETWORK_NAME+".con.xml"
TRAFFICLIGHT_FILE=NETWORK_LOCATION+NETWORK_NAME+".tra.xml"
NETCONFIG_FILE=NETWORK_LOCATION+NETWORK_NAME+".netccfg"
ROUTE_FILE=NETWORK_LOCATION+NETWORK_NAME+".rou.xml"
SUMOCONFIG_FILE=NETWORK_LOCATION+NETWORK_NAME+".sumocfg"