"""

This file generates a grid size*size network, with 3 lane dedicated turn edge and 2 lane normal edge.

All border nodes are designed to be dead-end.  All inside nodes are with traffic light.


The grid network is coordinated (x,y), y|__
										 x
The length of normal lanes are 200 meters and turnlane length is 50 meters


the grid node is labled as   xy, for example, the node in (1,1) is labeled as 11.  The intermediate node from 01 to 11 (node between a normal edge and a turn edge) is labeled as 0111.  the normal edge is 
labeled as l0111 and turn edge is labeled as c0111

some further flexibilities can be added to this generator to generate rectangle grids with different length of lanes


traffic light logic generation is also given

"""


LENGTH=200
TURNLANE=50
class gridNetworkGenerator:
	def generateGridNetwork(self,fileLocation1,fileLocation2,fileLocation3,size,numberOfLane,typeOfIntersection):
		#generate nodes
		nodeString="""<?xml version="1.0" encoding="UTF-8"?>
<nodes xmlns:xsi="http://www.w3.org/LENGTH1/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://sumo-sim.org/xsd/nodes_file.xsd">\n"""
		for i in range(size):
			for j in range(size):
				if i==0 or j==0 or i==size-1 or j==size-1:
					nodeString+="""<node id="%s" x="%f" y="%f" type="dead_end"  />\n""" %(str(i)+str(j),LENGTH*i,LENGTH*j)
				else:
					nodeString+="""<node id="%s" x="%f" y="%f" type="%s"  />\n""" %(str(i)+str(j),LENGTH*i,LENGTH*j,typeOfIntersection)
		nodeString+="""</nodes>"""
		f=open(fileLocation1,"w")
		
		f.write(nodeString)
		f.close()
		
		#generate edges
		edgeString="""<?xml version="1.0" encoding="UTF-8"?>
<edges xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://sumo-sim.org/xsd/edges_file.xsd">\n"""
		for i in range(1,size-1):
			for j in range(1,size-1):		
				edgeString+="""<edge id="%s" from="%s" to="%s" priority="1" numLanes="%i" speed="20" />\n""" %("l"+str(i-1)+str(j)+str(i)+str(j),str(i-1)+str(j),str(i)+str(j),numberOfLane)
				edgeString+="""<edge id="%s" from="%s" to="%s" priority="1" numLanes="%i" speed="20" />\n""" %("l"+str(i+1)+str(j)+str(i)+str(j),str(i+1)+str(j),str(i)+str(j),numberOfLane)
				edgeString+="""<edge id="%s" from="%s" to="%s" priority="1" numLanes="%i" speed="20" />\n""" %("l"+str(i)+str(j-1)+str(i)+str(j),str(i)+str(j-1),str(i)+str(j),numberOfLane)
				edgeString+="""<edge id="%s" from="%s" to="%s" priority="1" numLanes="%i" speed="20" />\n""" %("l"+str(i)+str(j+1)+str(i)+str(j),str(i)+str(j+1),str(i)+str(j),numberOfLane)
				print i,j
				if i==1:
					edgeString+="""<edge id="%s" from="%s" to="%s" priority="1" numLanes="%i" speed="20" />\n""" %("l"+str(i)+str(j)+str(i-1)+str(j),str(i)+str(j),str(i-1)+str(j),numberOfLane)
				if i==size-2:
					edgeString+="""<edge id="%s" from="%s" to="%s" priority="1" numLanes="%i" speed="20" />\n""" %("l"+str(i)+str(j)+str(i+1)+str(j),str(i)+str(j),str(i+1)+str(j),numberOfLane)
				if j==1:
					edgeString+="""<edge id="%s" from="%s" to="%s" priority="1" numLanes="%i" speed="20" />\n""" %("l"+str(i)+str(j)+str(i)+str(j-1),str(i)+str(j),str(i)+str(j-1),numberOfLane)
				if j==size-2:
					edgeString+="""<edge id="%s" from="%s" to="%s" priority="1" numLanes="%i" speed="20" />\n""" %("l"+str(i)+str(j)+str(i)+str(j+1),str(i)+str(j),str(i)+str(j+1),numberOfLane)


		edgeString+="""</edges>"""
		print edgeString
		f=open(fileLocation2,"w")
		f.write(edgeString)
		f.close()
		
		#generate connections
		connString="""<?xml version="1.0" encoding="iso-8859-1"?>
<connections xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://sumo-sim.org/xsd/connections_file.xsd">"""
		

		for i in range(1,size-1):
			for j in range(1,size-1):				
				#intersection connection
				for k in range(1,numberOfLane-1):
					connString+="""<connection from="%s" to="%s" fromLane="%i" toLane="%i"/>\n"""%("l"+str(i-1)+str(j)+str(i)+str(j),"l"+str(i)+str(j)+str(i+1)+str(j),k,k)		
				connString+="""<connection from="%s" to="%s" fromLane="%i" toLane="%i"/>\n"""%("l"+str(i-1)+str(j)+str(i)+str(j),"l"+str(i)+str(j)+str(i)+str(j-1),0,0)		
				connString+="""<connection from="%s" to="%s" fromLane="%i" toLane="%i"/>\n"""%("l"+str(i-1)+str(j)+str(i)+str(j),"l"+str(i)+str(j)+str(i)+str(j+1),numberOfLane-1,numberOfLane-1)	

				for k in range(1,numberOfLane-1):
					connString+="""<connection from="%s" to="%s" fromLane="%i" toLane="%i"/>\n"""%("l"+str(i+1)+str(j)+str(i)+str(j),"l"+str(i)+str(j)+str(i-1)+str(j),k,k)		
				connString+="""<connection from="%s" to="%s" fromLane="%i" toLane="%i"/>\n"""%("l"+str(i+1)+str(j)+str(i)+str(j),"l"+str(i)+str(j)+str(i)+str(j+1),0,0)		
				connString+="""<connection from="%s" to="%s" fromLane="%i" toLane="%i"/>\n"""%("l"+str(i+1)+str(j)+str(i)+str(j),"l"+str(i)+str(j)+str(i)+str(j-1),numberOfLane-1,numberOfLane-1)


				for k in range(1,numberOfLane-1):
					connString+="""<connection from="%s" to="%s" fromLane="%i" toLane="%i"/>\n"""%("l"+str(i)+str(j+1)+str(i)+str(j),"l"+str(i)+str(j)+str(i)+str(j-1),k,k)		
				connString+="""<connection from="%s" to="%s" fromLane="%i" toLane="%i"/>\n"""%("l"+str(i)+str(j+1)+str(i)+str(j),"l"+str(i)+str(j)+str(i+1)+str(j),numberOfLane-1,numberOfLane-1)		
				connString+="""<connection from="%s" to="%s" fromLane="%i" toLane="%i"/>\n"""%("l"+str(i)+str(j+1)+str(i)+str(j),"l"+str(i)+str(j)+str(i-1)+str(j),0,0)

				for k in range(1,numberOfLane-1):
					connString+="""<connection from="%s" to="%s" fromLane="%i" toLane="%i"/>\n"""%("l"+str(i)+str(j-1)+str(i)+str(j),"l"+str(i)+str(j)+str(i)+str(j+1),k,k)		
				connString+="""<connection from="%s" to="%s" fromLane="%i" toLane="%i"/>\n"""%("l"+str(i)+str(j-1)+str(i)+str(j),"l"+str(i)+str(j)+str(i-1)+str(j),numberOfLane-1,numberOfLane-1)		
				connString+="""<connection from="%s" to="%s" fromLane="%i" toLane="%i"/>\n"""%("l"+str(i)+str(j-1)+str(i)+str(j),"l"+str(i)+str(j)+str(i+1)+str(j),0,0)
				
		connString+="""</connections>"""
		#print connString
		f=open(fileLocation3,"w")
		f.write(connString)
		f.close()				
		
	#generate default traffic light logic for each intersection	
	def generateTrafficLightLogic(self,fileLocation,size):
		lightString="<additional>"
		for i in range(1,size-1):
			for j in range(1,size-1):
				lightString+="""<tlLogic id="%s" programID="my_program" offset="0" type="static">
   						<phase duration="30" state="rrrGGrrrrGGr"/>
  						<phase duration="30" state="GGrrrrGGrrrr"/>
  						<phase duration="20" state="rrrrrGrrrrrG"/>
   						<phase duration="20" state="rrGrrrrrGrrr"/>
						</tlLogic>\n"""%(str(i)+str(j))	
		lightString+="</additional>"		
		f=open(fileLocation,"w")
		f.write(lightString)
		f.close()			
		
