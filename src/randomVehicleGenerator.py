"""
Be careful the encoding part
.encode(encoding='UTF-8')

all strings that will be sent through socket need to change to UTF8

This class randomly generates vehicles

p_arrive is the arrival probability

The vehicle is generated in a normal edge (not in a turn edge), in a random lane

The vehicle is then given a randomly generated route.

The lane change mode of vehicle is important here, need further tuning. A flexible lane change mode makes vehicle to always change lanes  (even in the turn edge). A rigid lane change mode makes vehicle
to strucked in a wrong lane and cannot change

"""
from constant import *
import sys,os
#python encoding is not correct
reload(sys)
sys.setdefaultencoding("utf8")
# we need to import python modules from the $SUMO_HOME/tools directory
sys.path.append(os.path.join(os.environ['SUMO_HOME'], 'tools')) #
import traci
import random


class randomVehicleGenerator:
	def __init__(self,nx,p_arrive_automated,p_arrive_cooperative,p_arrive_legacy,p_left,p_right,p_straight,p_exit):
		self.nx=nx
		self.p_arrive_automated=p_arrive_automated
		self.p_arrive_cooperative=p_arrive_cooperative
		self.p_arrive_legacy=p_arrive_legacy
		self.p_arrive=p_arrive_automated+p_arrive_cooperative+p_arrive_legacy
		self.p_left=p_left
		self.p_right=p_right
		self.p_straight=p_straight
		self.p_exit=p_exit
		self.count=0
	
		
	def generateVehicle(self):
		for node in self.nx.nodes:
			if node.typ=="dead_end":
				edges=self.nx.nodeOutEdgeDic[node.nid]
				for startEdge in edges:
					if startEdge.eid.find('l')>=0: 
						rand=random.uniform(0,1)
						if rand<=self.p_arrive:
							
							#randomly generate vehicles of different type
							vehicleType=""
							if rand<=self.p_arrive_automated:
								vehicleType="automated"
							elif rand<=self.p_arrive_automated+self.p_arrive_cooperative:
								vehicleType="cooperative"
							elif rand<=self.p_arrive:
								vehicleType="legacy"	
							
							#randomly generate routes
							route=[startEdge.eid.encode(encoding='UTF-8')]
							rand=random.uniform(0,1)
							thisEdgeID=startEdge.eid
							lane=0
							while rand>self.p_exit:
								if not thisEdgeID in self.nx.connection:
									break
								#turn left
								if rand<=self.p_exit+self.p_left:
									lane=laneNumber-1
									for edge in self.nx.connection[thisEdgeID]:
										if edge[1]=='l':
											nextEdgeID=edge[0]
											route.append(nextEdgeID.encode(encoding='UTF-8'))
											thisEdgeID=nextEdgeID
											break
								#turn right
								elif rand<=self.p_exit+self.p_left++self.p_right:
									lane=0
									for edge in self.nx.connection[thisEdgeID]:
										if edge[1]=='r':
											nextEdgeID=edge[0]
											route.append(nextEdgeID.encode(encoding='UTF-8'))
											thisEdgeID=nextEdgeID
											lane=0
											break	
								#straight								
								elif rand<=1:
									lane=random.randint(1,max(laneNumber-2,1))
									for edge in self.nx.connection[thisEdgeID]:
										if edge[1]=='s':
											nextEdgeID=edge[0]
											route.append(nextEdgeID.encode(encoding='UTF-8'))
											thisEdgeID=nextEdgeID
											break								
								rand=random.uniform(0,1)
							traci.vehicle.add(str(self.count),startEdge.eid.encode(encoding='UTF-8'),pos=0,lane=lane,typeID=vehicleType)													
							traci.vehicle.setRoute(str(self.count),route)	
							#vehicle lane change mode
							traci.vehicle.setLaneChangeMode(str(self.count),int('0000000000',2))
							#traci.vehicle.setLaneChangeMode(str(self.count),int('1111111111',2))
							
					self.count+=1
