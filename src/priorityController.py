from network import network
from aim_models import *
import networkx 
from matplotlib import pyplot
class priorityController:
    def __init__(self,traci,network):
        self.traci=traci
        self.network=network
        self.priorityGraphDictionary={}
        for node in self.network.nodes:
            self.priorityGraphDictionary[node.nid]=priorityGraph()
    def control(self):
        for node in self.network.intersection:
            self.__nodeControl(node)
    def __nodeControl(self,node):      
        pGraph=self.priorityGraphDictionary[node.nid]  
        for edge in self.network.nodeInEdgeDic[node.nid]:
            for lane in edge.lanes:
                arc=self.network.returnArcByLaneID(lane.lid)

                #add new vehicles
                detectorID=lane.detector.did
                vehicleID=None
                if len(self.traci.inductionloop.getLastStepVehicleIDs(detectorID.encode(encoding='UTF-8')))>0:
                    vehicleID=self.traci.inductionloop.getLastStepVehicleIDs(detectorID.encode(encoding='UTF-8'))[0]
                if vehicleID!=None:                    
                    veh=vehicle(self.traci,vehicleID,arc)
                    if not self.network.isInNetwork(veh):
                        arc.addVehicle(veh)
                        self.network.addVehicle(veh)
                        #assign priority
                        pGraph.naivePriority(veh)
                        #print networkx.simple_cycles(pGraph.graph)[0]

                #remove out vehicle
                #set new speed for vehicle with regarding their priority and position
        for v in self.network.vehicles:
            if v.getPosition()<0:
                v.setSpeedTo(MAX_SPEED)
                v.arc.delVehicle(v)
                self.network.delVehicle(v)
                pGraph.removeVehicle(v.id)
        for v in self.network.vehicles:
            v.clearCommand()
        for cp in self.network.cpoints:
            arc1=cp.arc1
            arc2=cp.arc2
            v1_2=arc1.getTwoVehicleBeforeLength(cp.relativePosArc1)
            v2_2=arc2.getTwoVehicleBeforeLength(cp.relativePosArc2)
            """pV1=""
            pV2=""
            for v in arc1.vehicles:
                pV1+=str(v.getPosition())+","
            for v in arc2.vehicles:
                pV2+=str(v.getPosition())+","
            if v1!=None and v2!=None and arc1.startLane.lid=="l0111_1" and arc2.startLane.lid=="l1011_1":
                print "arc",arc1.startLane.lid,arc2.startLane.lid
                print "vehicle",v1.getPosition(),v2.getPosition()
                print "cp",cp.relativePosArc1,cp.relativePosArc2
                print "position all vehicles",arc1.startLane.lid,pV1
                print "position all vehicles",arc2.startLane.lid,pV2"""

            if v1_2!=None and v2_2==None:
                for v1 in v1_2:
                    v1.setCommand(1)
            elif v1_2==None and v2_2!=None:
                for v2 in v2_2:
                    v2.setCommand(1)
            elif v1_2!=None and v2_2!=None:
                for v1 in v1_2:
                    for v2 in v2_2:
                        if pGraph.isHigherPriority(v1,v2):
                            if cp.returnRelativePos(arc2)-v2.getPosition()-(COLLISION_R+THRESHOLD)<=0.5*v2.getSpeed()*v2.getSpeed()/DCC:
                                    v2.setCommand(0)
                            v1.setCommand(1)
                        else:
                            if cp.returnRelativePos(arc1)-v1.getPosition()-(COLLISION_R+THRESHOLD)<=0.5*v1.getSpeed()*v1.getSpeed()/DCC:
                                    v1.setCommand(0)
                            v2.setCommand(1)
        for v in self.network.vehicles:
            v.setSpeed()   
