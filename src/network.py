#!/usr/bin/env python
"""
This class represents network

"""
from xml.dom import minidom
from detector import detector,ildetector,e3detector
from node import node
from edge import edge
from lane import lane
from aim_models import arc,collisionPoint
import networkx 
from matplotlib import pyplot

from constant import *		

		
class network:
	
	def __init__(self,directory,networkName):
		
		self.directory=directory
		self.networkName=networkName
		
		
		self.nodes=list()
		self.intersection=list()#intersections with priorities
		self.edges=list()
		self.detectors=list()
		
		
		#some dictionaries to map 
		self.nodeInEdgeDic={}#each node id is mapped to a list of edges
		self.nodeOutEdgeDic={}
		self._lane_to_e3detectorID={}
		self.connection={}
		
		self.vehicles=list()
		#arcs and collision points
		self.arcs=list()
		self.cpoints=list()
	def addVehicle(self,veh):
		if not self.isInNetwork(veh):
			self.vehicles.append(veh)
	def delVehicle(self,v):
		for v1 in self.vehicles:
			if v1.id==v.id:
				self.vehicles.remove(v1)
	def isInNetwork(self,veh):
		if len(self.vehicles)>0:	
			for v in self.vehicles:
				if v.id==veh.id:
					return True	

	def parseNetwork(self):
		directory=self.directory
		networkName=self.networkName
		#parse directory/networkName.net.xml into internal representation
		netxml=minidom.parse(directory+networkName+".net.xml")
		#parse node, here we care only normal node (not internal nodes)
		nodelist=netxml.getElementsByTagName('junction')
		for nodeElement in nodelist:
			x=nodeElement.getAttribute('x')
			y=nodeElement.getAttribute('y')
			typ=nodeElement.getAttribute('type')
			if not nodeElement.getAttribute('id').find(':')>=0:
				n=node(nodeElement.getAttribute('id'),(float(x),float(y)),typ)
				self.nodes.append(n)	
				if typ=="priority" or typ=="traffic_light":
					self.intersection.append(n)
		#parse edge, her we care only about normal edges, not internal edges
		edgelist=netxml.getElementsByTagName('edge')
		#comment for myself: should not use [].append not working
		for edgeElement in edgelist:
			eid=edgeElement.getAttribute('id')
			fromm=edgeElement.getAttribute('from')
			to=edgeElement.getAttribute('to')
			laneList=list()
			for laneElement in edgeElement.childNodes:
				if not isinstance(laneElement,minidom.Text):
					lid=laneElement.getAttribute('id')
					length=float(laneElement.getAttribute('length'))
					shape=laneElement.getAttribute('shape')
					l=lane(lid,length,shape)
					laneList.append(l)
			e=edge(eid,fromm,to,laneList)
			self.edges.append(e)
			
			#add to node edge mapping dictionaries
			if not edgeElement.getAttribute('from') in self.nodeOutEdgeDic:
				l=list()
				l.append(e)
				self.nodeOutEdgeDic[edgeElement.getAttribute('from')]=l

			else:
				l=self.nodeOutEdgeDic[edgeElement.getAttribute('from')]
				l.append(e)
				self.nodeOutEdgeDic[edgeElement.getAttribute('from')]=l
			if not edgeElement.getAttribute('to') in self.nodeInEdgeDic:
				l=list()
				l.append(e)
				self.nodeInEdgeDic[edgeElement.getAttribute('to')]=l
			else:
				l=self.nodeInEdgeDic[edgeElement.getAttribute('to')]
				l.append(e)
				self.nodeInEdgeDic[edgeElement.getAttribute('to')]=l
					
		#parse connections
		connlist=netxml.getElementsByTagName('connection')
		for connElement in connlist:
			fromEdgeID=connElement.getAttribute('from')
			if not fromEdgeID.find(':')>=0:
				to=connElement.getAttribute('to')
				fromLane=connElement.getAttribute('fromLane')
				toLane=connElement.getAttribute('toLane')
				direction=connElement.getAttribute('dir')
				via=connElement.getAttribute('via')
				if not fromEdgeID in self.connection:
					l=list()
					l.append([to,direction,fromLane,toLane])
					self.connection[fromEdgeID]=l

				else:
					l=self.connection[fromEdgeID]
					l.append([to,direction,fromLane,toLane])
					self.connection[fromEdgeID]=l

				#parse arc
				if via!=None:
					startLane=self.getLanebyID(fromEdgeID+'_'+fromLane)
					endLane=self.getLanebyID(to+'_'+toLane)
					internalLane=self.getLanebyID(via)
					self.arcs.append(arc(startLane,internalLane))
		#define arcs and its collision points 			
		for arc1 in self.arcs:
			for arc2 in self.arcs:				
				if arc1!=arc2:
					cp=collisionPoint(arc1,arc2)
					if cp.getCollisionPointCord()!=None:
						self.cpoints.append(cp)

		for arc1 in self.arcs:
			arc1.collisionPointList.sort(key=lambda cp: cp.returnRelativePos(arc1),reverse=True)
	def getEdgebyID(self,eid):
		for edge in self.edges:
			if edge.eid==eid:
					return edge
	def getLanebyID(self,lid):
		for edge in self.edges:
			for lane in edge.lanes:
				if lane.lid==lid:
					return lane
	def getNodebyID(self,nid):
		for node in self.nodes:
			if node.nid==nid:
				return node		
				
				
	#manage different detectors				
	def addInductionLoopDetector(self,did,lane,pos,frequency,output):
		detc=ildetector(did,lane,pos,frequency,output)
		self.detectors.append(detc)
		return detc
	def addE3detector(self,did,entryLane,entryPos,exitLane,exitPos,freq,output):
		detc=e3detector(did,entryLane,entryPos,exitLane,exitPos,freq,output)
		self._lane_to_e3detectorID[entryLane]=did
		self.detectors.append(detc)

	def deployDetectors(self):	
		fi=open(self.directory+"/"+self.networkName+".det.xml","w")
		detectorString="""<additional>\n"""
		for detc in self.detectors:
			detectorString+=detc.toXMLString()
		detectorString+="""</additional>"""
		fi.write(detectorString)
		fi.close()
	def getE3DetectorIDFromLane(self,lane):
		return 	self._lane_to_e3detectorID[lane]

	def returnArcByLaneID(self,laneID):
		for arc in self.arcs:
			if arc.startLane.lid==laneID or arc.internalLane.lid==laneID:
				return arc
					
def giveName(test):
	return str(test[0])+str(test[1])


		
if __name__=="__main__":
	nx=network('../data/multiIntersection/','cross')
	g=networkx.Graph()
	nx.parseNetwork()
	for n in nx.nodes:
		#print n
		pass
	for e in nx.edges:
		#print e
		pass
	for a in nx.arcs:
		a.collisionPointList.sort(key=lambda cp: cp.returnRelativePos(a),reverse=True)
		print len(a.collisionPointList),"ok"
		seg1=a.getCollidingSegments()
		print seg1
		"""g.add_node(giveName(seg1[0]),pos=seg1[0])			
		for i in range(0,len(seg1)-1):
			g.add_node(giveName(seg1[i+1]),pos=seg1[i+1])
			g.add_edge(giveName(seg1[i]),giveName(seg1[i+1]))"""
	

	print len(nx.cpoints)
	j=0
	for c in nx.cpoints:
		print c.arc1.startLane.lid,c.arc2.startLane.lid,c.returnRelativePos(c.arc1),c.returnRelativePos(c.arc2)
		seg1=c.arc1.getCollidingSegments()
		seg2=c.arc2.getCollidingSegments()
		
		g.add_node(giveName(seg1[0]),pos=seg1[0])	
		for i in range(0,len(seg1)-1):
			g.add_node(giveName(seg1[i+1]),pos=seg1[i+1])
			g.add_edge(giveName(seg1[i]),giveName(seg1[i+1]))
	
		g.add_node(giveName(seg2[0]),pos=seg2[0])
		for i in range(0,len(seg2)-1):
			g.add_node(giveName(seg2[i+1]),pos=seg2[i+1])
			g.add_edge(giveName(seg2[i]),giveName(seg2[i+1]))	
		#if g.
		g.add_node(str(c.x)+str(c.y),pos=(c.x, c.y))
		#print g.nodes()
	pos=networkx.get_node_attributes(g,'pos')
	networkx.draw(g,pos,with_labels=False,node_size=5)
	pyplot.draw()
	pyplot.show()
