NROUTE=100
import random
class routeGenerator:
	def __init__(self,nx,fileLocation,timeInSecond,resolution):
		self.fileLocation=fileLocation
		self.timeInSecond=timeInSecond
		self.resolution=resolution
		self.nx=nx
class routeGeneratorWithTurnProb(routeGenerator):    #only works for cross(+) for now, need further thinking for "strange" intersections
	def __init__(self,nx,fileLocation,timeInSecond=0,simulation_step_time=0,p_arrive=0,p_left=0,p_right=0,p_straight=0,p_exit=0):
		routeGenerator.__init__(self,nx,fileLocation,timeInSecond,simulation_step_time)
		self.p_left=p_left
		self.p_right=p_right
		self.p_straight=p_straight
		self.p_exit=p_exit
		self.p_arrive=p_arrive


	def generateDefaultRoute(self):
		routeFileOutput="""<routes>
			<vType id="automated" accel="100" decel="100" sigma="0.5" length="5" minGap="2.5" maxSpeed="15" guiShape="passenger" color="white"/>
			<vType id="cooperative" accel="100" decel="100" sigma="0.5" length="5" minGap="2.5" maxSpeed="15" guiShape="passenger" color="yellow"/>
			<vType id="legacy" accel="100" decel="100" sigma="0.5" length="5" minGap="2.5" maxSpeed="15" guiShape="passenger" color="red"/>""" 

		for edge in self.nx.edges:
			if edge.eid.find('l')>=0:
				routeFileOutput+="<route id=\""+edge.eid+"\" edges=\""+edge.eid+"\" />\n"
		routeFileOutput+= "</routes>"
		f=open(self.fileLocation,"w")
		f.write(routeFileOutput)
		f.close()
		