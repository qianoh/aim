from lane import lane
import math
import networkx 

from constant import *
#Be really careful that how we compare different numbers, we should add a ERROR marge for float
ERROR=0.000001


class arc:
	def __init__(self,startLane,internalLane):
		self.startLane=startLane
		self.internalLane=internalLane
		self.collisionPointList=list()
		self.vehicles=list()
		self.length=START_LANE_LENGTH+self.internalLane.length
	#set collision point list
	def addCollisionPoint(self,c):
		if len(self.collisionPointList)>0:
			for cp in self.collisionPointList:
				if cp.getCollisionPointCord()==c.getCollisionPointCord():
					return
		self.collisionPointList.append(c)	
	def getCollisionPointListInFront(self,position):
		InFrontCollisionPointList=list()
		for c in self.collisionPointList:
			if c.returnRelativePos(self)>position:
				InFrontCollisionPointList.append(c)
		return InFrontCollisionPointList
	#return a set of points
	def getCollidingSegments(self):
		return self.internalLane.points
	"""def buildCollisionPoint(otherArc):
		thisSegments=self.getSuspiciousCollidingSegments()
		otherSegments=otherArc.getSuspiciousCollidingSegments()
		for i in range(0,len(thisSegments)-1):
			for j in range(0,len(otherSegments)-1):"""
	def hasVehicle(self,vehicle):
		for v in self.vehicles:
			if v.id==vehicle.id:
				return True
	def addVehicle(self,vehicle):
		if not self.hasVehicle(vehicle):
			self.vehicles.append(vehicle)
			self.vehicles.sort(key=lambda vehicle: vehicle.getPosition(),reverse=True)				
	def delVehicle(self,vehicle):
		self.vehicles.remove(vehicle)
	
	def getTwoVehicleBeforeLength(self,cPosition):
		for i in range(0,len(self.vehicles)):
			if self.vehicles[i].getPosition()<cPosition+THRESHOLD:
				if len(self.vehicles)-1==i:
					return [self.vehicles[i]]
				else:
					return [self.vehicles[i],self.vehicles[i+1]]
	def __str__(self):
		return str(self.internalLane.points)


class collisionPoint:
	def __init__(self,arc1,arc2):
		self.arc1=arc1
		self.arc2=arc2
		self.x=None
		self.y=None
		self.relativePosArc1=None
		self.relativePosArc2=None
		self.arc1DistanceSpeedRelationDic={}
		self.arc2DistanceSpeedRelationDic={}
		self.__calculateCollisionPoint()
		self.__calculateRelativePos()
		self.__initializeDistanceSpeedDic()

	def __initializeDistanceSpeedDic(self):
		currentPos=0.0
		while currentPos<self.relativePosArc1:
			self.arc1DistanceSpeedRelationDic[int(currentPos/MIN_INTERVAL)]=min(math.sqrt(2.0*DCC*max((self.relativePosArc1-currentPos-THRESHOLD),0.0)),MAX_SPEED)
			currentPos+=MIN_INTERVAL
		currentPos=0.0
		while currentPos<self.relativePosArc2:
			self.arc2DistanceSpeedRelationDic[int(currentPos/MIN_INTERVAL)]=min(math.sqrt(2.0*DCC*max((self.relativePosArc2-currentPos-THRESHOLD),0.0)),MAX_SPEED)
			currentPos+=MIN_INTERVAL
	def getOtherArc(self,arc):
		if arc==self.arc1:
			return self.arc2
		if arc==self.arc2:
			return self.arc1
		return None
		
	def getCollisionPointCord(self):
		if self.x!=None and self.y!=None:
			return (self.x,self.y)
		else:
			return None

			
	def __calculateCollisionPoint(self):
		segments1=self.arc1.getCollidingSegments()
		segments2=self.arc2.getCollidingSegments()
		x=None
		y=None
		#use math to compute...
		for i in range(0,len(segments1)-1):
			for j in range(0,len(segments2)-1):
				A1=segments1[i+1][1]-segments1[i][1]
				B1=segments1[i][0]-segments1[i+1][0]
				C1=A1*segments1[i][0]+B1*segments1[i][1]
				A2=segments2[j+1][1]-segments2[j][1]
				B2=segments2[j][0]-segments2[j+1][0]
				C2=A2*segments2[j][0]+B2*segments2[j][1]
			
				det=A1*B2-A2*B1
				if(det==0):
					pass#line is parallel
				else:
					x=(B2*C1-B1*C2)/det
					y=(A1*C2-A2*C1)/det
		#verify if this point is inside both segment
				if x!=None and y!=None:
					if self.__checkPointPosition(x,y,segments1[i],segments1[i+1],segments2[j],segments2[j+1]):
						self.x=x
						self.y=y
		#we assume here only one intersection between two arcs
		
	def __checkPointPosition(self,x,y,segments1_start,segments1_end,segments2_start,segments2_end):
		if x>=min(segments1_start[0],segments1_end[0])*(1-ERROR) and x<=max(segments1_start[0],segments1_end[0])*(1+ERROR):
			if x>=min(segments2_start[0],segments2_end[0])*(1-ERROR) and x<=max(segments2_start[0],segments2_end[0])*(1+ERROR):
				if y>=min(segments1_start[1],segments1_end[1])*(1-ERROR) and y<=max(segments1_start[1],segments1_end[1])*(1+ERROR):
					if y>=min(segments2_start[1],segments2_end[1])*(1-ERROR) and y<=max(segments2_start[1],segments2_end[1])*(1+ERROR):
						return True
		return False
									
	def __calculateRelativePos(self):
		if self.x==None or self.y==None:
			return
		x=self.x
		y=self.y
		segments1=self.arc1.getCollidingSegments()
		segments2=self.arc2.getCollidingSegments()
		lenArc1=0
		lenArc2=0
		arc1_segment=-1
		arc2_segment=-1
		for i in range(0,len(segments1)-1):
			for j in range(0,len(segments2)-1):
				if self.__checkPointPosition(x,y,segments1[i],segments1[i+1],segments2[j],segments2[j+1]):
					arc1_segment=i
					arc2_segment=j
		if arc1_segment!=-1 and arc2_segment!=-1:
			for i in range(0,arc1_segment):
				lenArc1+=math.sqrt(math.pow((segments1[i+1][0]-segments1[i][0]),2)+math.pow((segments1[i+1][1]-segments1[i][1]),2))
			lenArc1+=math.sqrt(math.pow((x-segments1[arc1_segment][0]),2)+math.pow((y-segments1[arc1_segment][1]),2))
			for j in range(0,arc2_segment):
					lenArc2+=math.sqrt(math.pow((segments2[j+1][0]-segments2[j][0]),2)+math.pow((segments2[j+1][1]-segments2[j][1]),2))			
			lenArc2+=math.sqrt(math.pow((x-segments2[arc2_segment][0]),2)+math.pow((y-segments2[arc2_segment][1]),2))
			self.relativePosArc1=lenArc1+START_LANE_LENGTH
			self.relativePosArc2=lenArc2+START_LANE_LENGTH
	def returnRelativePos(self,arc):
		pos=None
		if arc==self.arc1:
			pos=self.relativePosArc1
		elif arc==self.arc2:
			pos=self.relativePosArc2
		if pos!=None:
			return pos
		else:
			return None
		
		
class vehicle:
	def __init__(self,traci,id,arc):
		self.id=id
		self.pos=0
		self.traci=traci
		self.acc=self.traci.vehicle.getAccel(self.id)
		self.dcc=self.traci.vehicle.getDecel(self.id)
		self.speed=self.traci.vehicle.getSpeed(self.id)
		self.arc=arc
		self.command=1
	def refreshVehicleState(self):
		#relative position
		vehiclePositionOnLane=self.traci.vehicle.getLanePosition(self.id)
		currentLaneID=self.traci.vehicle.getLaneID(self.id)
		if currentLaneID==self.arc.startLane.lid:
			self.pos=START_LANE_LENGTH-(self.arc.startLane.length-vehiclePositionOnLane)
		elif currentLaneID==self.arc.internalLane.lid:
			self.pos=vehiclePositionOnLane+START_LANE_LENGTH
		else:
			self.pos=-1
		#speed	
		self.speed=self.traci.vehicle.getSpeed(self.id)		
	def setSpeed(self):
		if self.command==1:
			self.traci.vehicle.setSpeed(self.id,self.getSpeed()+ACC/STEPPERSECOND)
		else:
			self.traci.vehicle.setSpeed(self.id,max(self.getSpeed()-(DCC+1)/STEPPERSECOND,0))
	def setSpeedTo(self,v):
		self.traci.vehicle.setSpeed(self.id,v)
			
	def getSpeed(self):
		self.refreshVehicleState()
		return self.speed
	def getPosition(self):
		self.refreshVehicleState()
		return self.pos
		#return self.traci.vehicle.getLanePosition(self.id)
	def clearCommand(self):
		self.command=1
	def setCommand(self,command):
		self.command=self.command*command
	
class priorityGraph:
	def __init__(self):
		self.graph=networkx.DiGraph()
	def addPriority(self,v1ID,v2ID):
		if(not self.graph.has_node(v1ID)):
			self.graph.add_node(v1ID)
		if(not self.graph.has_node(v2ID)):
			self.graph.add_node(v2ID)
		self.graph.add_edge(v1ID,v2ID)
	def removeVehicle(self,vID):
		self.graph.remove_node(vID)
	def isHigherPriority(self,v1,v2):
		v1ID=v1.id
		v2ID=v2.id
		if self.graph.has_edge(v1ID, v2ID):
			return True		
	def naivePriority(self,v):
		if len(self.graph.nodes())<1:
			self.graph.add_node(v.id)			
		else:
			for vehicleID in self.graph.nodes():
				if vehicleID!=v.id:
					self.addPriority(vehicleID, v.id)
							
