import math	
class edge:
	def __init__(self,eid,from_node,to_node,lanes):
		self.eid=eid
		self.from_node=from_node
		self.to_node=to_node
		self.lanes=lanes
	def __str__(self):
		return str( self.eid)
		
	#this function is not used for now
	def compareAngleTo(self,other):
		thisLaneShape=self.lanes[0].shape
		thisLanePointList=thisLaneShape.split(' ')
		thisLaneLastTwoPoints=[thisLanePointList[0],thisLanePointList[len(thisLanePointList)-1]]
		otherLaneShape=other.lanes[0].shape
		otherLanePointList=otherLaneShape.split(' ')
		otherLaneLastTwoPoints=[otherLanePointList[0],otherLanePointList[len(otherLanePointList)-1]]
		#ccordinates of two points to caculate angle
		thisX1=float(thisLaneLastTwoPoints[0].split(',')[0])
		thisY1=float(thisLaneLastTwoPoints[0].split(',')[1])
		thisX2=float(thisLaneLastTwoPoints[1].split(',')[0])
		thisY2=float(thisLaneLastTwoPoints[1].split(',')[1])		
		
		otherX1=float(otherLaneLastTwoPoints[0].split(',')[0])
		otherY1=float(otherLaneLastTwoPoints[0].split(',')[1])
		otherX2=float(otherLaneLastTwoPoints[1].split(',')[0])
		otherY2=float(otherLaneLastTwoPoints[1].split(',')[1])		

		#calculate this lane's angle
		thisAngle=0
		thisAngle=math.atan2((thisY2-thisY1),(thisX2-thisX1))	
		#calculate other lane's angle
		otherAngle=0
		otherAngle=math.atan2((otherY2-otherY1),(otherX2-otherX1))			
		return otherAngle-thisAngle
