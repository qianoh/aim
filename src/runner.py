#!/usr/bin/env python
"""
careful:

when use traci command, all string should be re-encoded in UTF-8, or it will not work



"""
from randomVehicleGenerator import randomVehicleGenerator
import os, sys

# we need to import python modules from the $SUMO_HOME/tools directory
sys.path.append(os.path.join(os.environ['SUMO_HOME'], 'tools')) #


#python encoding is not correct
reload(sys)
import optparse
import subprocess
import random
from network import network
from gridNetworkGenerator import gridNetworkGenerator
from priorityController import *

from constant import *

from sumolib import checkBinary
from route import routeGenerator,routeGeneratorWithTurnProb
import traci


from constant import *


	
def run(nx,priorityController):
    """execute the TraCI control loop"""
    traci.init(PORT)
    step = 0

    rVehicleGen=randomVehicleGenerator(nx,p_arrive_automated,p_arrive_cooperative,p_arrive_legacy,p_left,p_right,p_straight,p_exit)
    step=0
    #run TOTAL_RUN_TIME seconds
    while step<TOTAL_RUN_TIME*STEPPERSECOND:
        traci.simulationStep()
    #generate vehicles at the beginning of each second
        if step%STEPPERSECOND and step<int(TOTAL_VEHICLE_ARRIVE_TIME*STEPPERSECOND):
            rVehicleGen.generateVehicle()
        if step>=10:    
            priorityController.control()
        step+=1
    traci.close()
    sys.stdout.flush()

def get_options():
    optParser = optparse.OptionParser()
    optParser.add_option("--nogui", action="store_true", default=False, help="run the commandline version of sumo")
    options, args = optParser.parse_args()
    return options


# this is the main entry point of this script
if __name__ == "__main__":
    options = get_options()

    # this script has been called from the command line. It will start sumo as a
    # server, then connect and run
    if options.nogui:
        sumoBinary = checkBinary('sumo')
    else:
        sumoBinary = checkBinary('sumo-gui')
    #create network  
    gridG=gridNetworkGenerator()  
    gridG.generateGridNetwork(NODE_FILE,EDGE_FILE,CONNECTION_FILE,networkSize,laneNumber,typeOfIntersection)  
    #gridG.generateTrafficLightLogic(TRAFFICLIGHT_FILE,networkSize)      
    
    #compile network
    os.system('../../bin/netconvert '+ NETCONFIG_FILE)
    nx=network(NETWORK_LOCATION,NETWORK_NAME)
    nx.parseNetwork()
    for node in nx.intersection:
        for edge in nx.nodeInEdgeDic[node.nid]:
            for lane in edge.lanes:
                detector=nx.addInductionLoopDetector(lane.lid, lane.lid, lane.length-START_LANE_LENGTH, 60, lane.lid+" output.xml")
                lane.setDetector(detector)
    nx.deployDetectors()
    #generate the default routing for this simulation  
    #this default routing will be replaced by traci dynamically generated routing
    fileLocation=ROUTE_FILE
    rg=routeGeneratorWithTurnProb(nx,fileLocation)
    rg.generateDefaultRoute()
    
    priorityController=priorityController(traci,nx)
    
    # this is the normal way of using traci. sumo is started as a
    # subprocess and then the python script connects and runs
    sumoProcess = subprocess.Popen([sumoBinary, "-c", SUMOCONFIG_FILE, "--tripinfo-output", "tripinfo.xml","--step-length",str(1.0/STEPPERSECOND)], stdout=sys.stdout, stderr=sys.stderr)
    run(nx,priorityController)
    sumoProcess.wait()