class lane:
	def __init__(self,lid,length,shape):
		self.lid=lid
		self.length=length
		self.shape=shape
		self.points=list()
		#compute the list of points from shape
		strPointList=self.shape.split(' ')
		for strPoint in strPointList:
			temp=strPoint.split(',')
			x=float(temp[0])
			y=float(temp[1])
			self.points.append((x,y))
		self._queue=0
		self.detector=None
	#set queue length by detector	
	def setDetector(self,d):
		self.detector=d
	#get approximate queue length by counting all vehicles on the lane using traci.lane module,  this result is the same as getQueueLengthByDetector if the detector covers large enough part of turn lane
	#but this method is simple, no need to deploy detectors
	def getQueueLengthByTraciLane(self, traci):
		return traci.lane.getLastStepVehicleNumber(self.lid.encode(encoding='UTF-8'))
